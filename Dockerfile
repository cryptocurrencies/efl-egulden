# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libevent-dev libleveldb-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/Electronic-Gulden-Foundation/egulden.git /opt/egulden
RUN cd /opt/egulden/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/egulden/ && \
    make

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libevent-dev libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r egulden && useradd -r -m -g egulden egulden
RUN mkdir /data
RUN chown egulden:egulden /data
COPY --from=build /opt/egulden/src/eguldend /usr/local/bin/
COPY --from=build /opt/egulden/src/egulden-cli /usr/local/bin/
USER egulden
VOLUME /data
EXPOSE 11015 21015
CMD ["/usr/local/bin/eguldend", "-datadir=/data", "-conf=/data/egulden.conf", "-server", "-txindex", "-printtoconsole"]